package com.pj.keycloak.web;

import com.pj.keycloak.model.Employee;
import com.pj.keycloak.service.EmployeeService;
import com.pj.keycloak.util.UserInfoUtil;

import org.keycloak.KeycloakPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/employee")
@CrossOrigin(origins = "http://localhost:4200/*")
public class EmployeeController {
	private final EmployeeService employeeService;
	private final UserInfoUtil userInfoUtil;

	private final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

	public EmployeeController(EmployeeService employeeService, UserInfoUtil userInfoUtil) {
		this.employeeService = employeeService;
		this.userInfoUtil = userInfoUtil;
	}

	@GetMapping(path = "/list")
	@RolesAllowed("ranes")
	public List<Employee> findAll(HttpServletRequest httpServletRequest) {

		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println(principal);

		KeycloakPrincipal data = (KeycloakPrincipal) principal;
		logger.info(data.getKeycloakSecurityContext().getToken().getName());
		Employee employee = new Employee();
		employee.setId(1L);
		
		List<Employee> employees = new ArrayList<>();
		employees.add(employee);
		logger.info("User Id: {}", userInfoUtil.getPreferredUsername(httpServletRequest));
		return employees;
	}

	@GetMapping(path = "/find/{id}")
	public Optional<Employee> findById(@PathVariable Long id) {
		return employeeService.findById(id);
	}

	@DeleteMapping(path = "/delete/{id}")
	public void deleteEmployee(@PathVariable Long id) {
		employeeService.deleteById(id);
	}
}
