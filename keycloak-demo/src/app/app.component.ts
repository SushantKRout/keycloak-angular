import { HttpClient, HttpClientModule } from "@angular/common/http";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent implements OnInit {
  title = "keycloak-demo";
  constructor(private http: HttpClient) {}
  ngOnInit() {
    this.http.get("http://localhost:8081/api/v1/employee/list").subscribe((res) => {
      console.log(res);
    });
  }
}
