import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmployeeComponent } from './employee/employee.component';
import { AuthGuard } from '../app/auth.guard'
import { DepartmentComponent } from './department/department.component';

const routes: Routes = [
  {
    path: '',
    redirectTo:'employee',
    pathMatch:'full'
  }, {
    path:'employee',
    component: EmployeeComponent,
    canActivate:[AuthGuard]
  }, {
    path:'department',
    component: DepartmentComponent,
    canActivate:[AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
