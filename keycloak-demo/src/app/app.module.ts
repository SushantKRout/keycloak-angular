import { BrowserModule } from "@angular/platform-browser";
import { APP_INITIALIZER, NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { KeycloakAngularModule, KeycloakService } from "keycloak-angular";
import { EmployeeComponent } from "./employee/employee.component";
import { DepartmentComponent } from "./department/department.component";
import { HttpClientModule } from "@angular/common/http";

function initializeKeycloak(keycloak: KeycloakService) {
  return () =>
    keycloak
      .init({
        config: {
          url: "http://localhost:8080/auth",
          realm: "keycloakdemo",
          clientId: "angular-app",
        },
        initOptions: {
          checkLoginIframe: true,
          checkLoginIframeInterval: 25,
          /* onLoad: 'check-sso', */
          /* silentCheckSsoRedirectUri:
          window.location.origin + '/assets/silent-check-sso.html', */
        },
      })
      .then((su) => {
        console.log(su);
      });
}

@NgModule({
  declarations: [AppComponent, EmployeeComponent, DepartmentComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    KeycloakAngularModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
